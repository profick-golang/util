package collections

type Set interface {
	Add(interface{}) bool
	Remove(interface{}) bool
	Contains(interface{}) bool
	Empty() bool
	Size() int
	Clear()
}
