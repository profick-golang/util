package treeset

import "strings"

var stringComparator = func(left, right interface{}) int {
	return strings.Compare(left.(string), right.(string))
}
