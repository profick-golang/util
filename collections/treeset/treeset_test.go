package treeset

import "testing"

func TestTreeSet_Size(t *testing.T) {
	tests := []struct {
		name     string
		values   []string
		wantSize int
	}{
		{"AddOneElement", []string{"1"}, 1},
		{"AddTwoElements", []string{"1", "2"}, 2},
		{"AddMultipleElements", []string{"3", "2", "4", "1", "5"}, 5},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			tree := NewWithStringComparator()
			for _, v := range test.values {
				tree.Add(v)
			}
			if gotSize := tree.Size(); gotSize != test.wantSize {
				t.Errorf("Size() = %v, want %v", gotSize, test.wantSize)
			}
		})
	}
}

func TestTreeSet_Size2(t *testing.T) {
	tree := NewWithStringComparator()
	tree.Add("1")
	tree.Add("2")
	tree.Remove("1")
	want := 1
	if got := tree.Size(); got != want {
		t.Errorf("Size() = %v, want %v", got, want)
	}
}

func TestTreeSet_Size3(t *testing.T) {
	tree := NewWithStringComparator()
	tree.Add("1")
	tree.Add("2")
	tree.Remove("1")
	tree.Remove("2")
	want := 0
	if got := tree.Size(); got != want {
		t.Errorf("Size() = %v, want %v", got, want)
	}
}

func TestTreeSet_Empty(t *testing.T) {
	tests := []struct {
		name   string
		values []string
		want   bool
	}{
		{"EmptyTree", []string{}, true},
		{"OneElementTree", []string{"1"}, false},
		{"MultipleElementsTree", []string{"3", "2", "4", "1", "5"}, false},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			tree := NewWithStringComparator()
			for _, v := range test.values {
				tree.Add(v)
			}
			if got := tree.Empty(); got != test.want {
				t.Errorf("Empty() = %v, want %v", got, test.want)
			}
		})
	}
}

func TestTreeSet_Empty2(t *testing.T) {
	tree := NewWithStringComparator()
	tree.Add("1")
	tree.Add("2")
	tree.Remove("1")
	want := false
	if got := tree.Empty(); got != want {
		t.Errorf("Empty() = %v, want %v", got, want)
	}
}

func TestTreeSet_Empty3(t *testing.T) {
	tree := NewWithStringComparator()
	tree.Add("1")
	tree.Add("2")
	tree.Remove("1")
	tree.Remove("2")
	want := true
	if got := tree.Empty(); got != want {
		t.Errorf("Empty() = %v, want %v", got, want)
	}
}

func TestTreeSet_Add(t *testing.T) {
	tree := NewWithStringComparator()

	if tree.root != nil {
		t.Error("New tree root should be null")
	}

	if added := tree.Add("1"); added == false {
		t.Error("Can't add value to the empty set")
	}

	if tree.root == nil {
		t.Error("Root node should be instantiated")
	}

	if tree.root.value != "1" {
		t.Errorf("Got %v expected %v as root value", tree.root.value, "3")
	}

	if added := tree.Add("1"); added == true {
		t.Error("Added already existed element and returned true")
	}
}

func TestTreeSet_Add2(t *testing.T) {
	tree := NewWithStringComparator()
	tree.Add("2")
	tree.Add("1")
	tree.Add("3")
	node2 := tree.root
	if node2 == nil {
		t.Error("Root should be instantiated")
		return
	}

	if node2.value != "2" {
		t.Errorf("Got %v expected %v as root value", node2.value, "2")
	}

	if node2.left == nil || node2.right == nil {
		t.Error("Root node should have two child nodes")
	}

	node1 := node2.left

	if node1.value != "1" {
		t.Errorf("Got %v expected %v as left child value", node1.value, "1")
	}

	if node1.parent != node2 {
		t.Error("Pointer to parent node is not set")
	}

	if node1.left != nil || node1.right != nil {
		t.Error("Leaf node has child node")
	}

	node3 := node2.right

	if node3.value != "3" {
		t.Errorf("Got %v expected %v as left child value", node3.value, "3")
	}

	if node3.parent != node2 {
		t.Error("Pointer to parent node is not set")
	}

	if node3.left != nil || node3.right != nil {
		t.Error("Leaf node has child node")
	}
}

func TestTreeSet_Clear(t *testing.T) {
	tests := []struct {
		name   string
		values []string
	}{
		{"EmptyTree", []string{}},
		{"OneElementTree", []string{"1"}},
		{"MultipleElementsTree", []string{"3", "2", "4", "1", "5"}},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			tree := NewWithStringComparator()
			for _, v := range test.values {
				tree.Add(v)
			}
			tree.Clear()
			if got := tree.Size(); got != 0 {
				t.Errorf("Size() = %v, want %v", got, 0)
			}
			if tree.root != nil {
				t.Error("Root should be nil after TreeSet#Clear() call")
			}
		})
	}
}

func TestTreeSet_Contains(t *testing.T) {
	tests := []struct {
		name    string
		values  []string
		contain string
		want    bool
	}{
		{"EmptyTree", []string{}, "1", false},
		{"OneElementTree1", []string{"1"}, "0", false},
		{"OneElementTree2", []string{"1"}, "1", true},
		{"MultipleElementsTree1", []string{"3", "2", "4", "1", "5"}, "1", true},
		{"MultipleElementsTree2", []string{"3", "2", "4", "1", "5"}, "2", true},
		{"MultipleElementsTree3", []string{"3", "2", "4", "1", "5"}, "3", true},
		{"MultipleElementsTree4", []string{"3", "2", "4", "1", "5"}, "4", true},
		{"MultipleElementsTree5", []string{"3", "2", "4", "1", "5"}, "5", true},
		{"MultipleElementsTree6", []string{"3", "2", "4", "1", "5"}, "6", false},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			tree := NewWithStringComparator()
			for _, v := range test.values {
				tree.Add(v)
			}
			if got := tree.Contains(test.contain); got != test.want {
				t.Errorf("Contains() = %v, want %v", got, test.want)
			}
		})
	}
}

func TestTreeSet_Remove(t *testing.T) {
	tests := []struct {
		name         string
		values       []string
		nodeToRemove string
		want         bool
	}{
		{"EmptyTree", []string{}, "1", false},
		{"OneElementTree1", []string{"1"}, "0", false},
		{"OneElementTree2", []string{"1"}, "1", true},
		{"MultipleElementsTree1", []string{"3", "2", "4", "1", "5"}, "0", false},
		{"MultipleElementsTree2", []string{"3", "2", "4", "1", "5"}, "1", true},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			tree := NewWithStringComparator()
			for _, v := range test.values {
				tree.Add(v)
			}

			if got := tree.Remove(test.nodeToRemove); got != test.want {
				t.Errorf("Remove() = %v, want %v", got, test.want)
			}

			if got := tree.Contains(test.nodeToRemove); got != false {
				t.Errorf("Contains() = %v, want %v", got, test.want)
			}
		})
	}
}
