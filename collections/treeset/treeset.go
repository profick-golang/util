package treeset

type TreeSet struct {
	root       *node
	comparator func(interface{}, interface{}) int
	size       int
}

func New(comparator func(interface{}, interface{}) int) *TreeSet {
	return &TreeSet{comparator: comparator}
}

func NewWithStringComparator() *TreeSet {
	return &TreeSet{comparator: stringComparator}
}

func (tree *TreeSet) Add(value interface{}) bool {
	return tree.insert(value, &tree.root)
}

func (tree *TreeSet) Remove(value interface{}) bool {
	return tree.remove(value)
}

func (tree *TreeSet) Contains(value interface{}) bool {
	return tree.search(value, tree.root) != nil
}

func (tree *TreeSet) Empty() bool {
	return tree.size == 0
}

func (tree *TreeSet) Size() int {
	return tree.size
}

func (tree *TreeSet) Clear() {
	tree.root = nil
	tree.size = 0
}

func (tree *TreeSet) insert(value interface{}, currentNodePointer **node) bool {
	currentNode := *currentNodePointer
	if currentNode == nil {
		tree.size++
		*currentNodePointer = &node{value: value}
		return true
	}

	for {
		comparatorResult := tree.comparator(value, currentNode.value)
		if comparatorResult < 0 {
			if currentNode.left == nil {
				tree.size++
				currentNode.left = &node{parent: currentNode, value: value}
				return true
			}
			currentNode = currentNode.left
		} else if comparatorResult > 0 {
			if currentNode.right == nil {
				tree.size++
				currentNode.right = &node{parent: currentNode, value: value}
				return true
			}
			currentNode = currentNode.right
		} else {
			return false
		}
	}
}

func (tree *TreeSet) search(value interface{}, currentNode *node) *node {
	if currentNode == nil {
		return nil
	}

	comparatorResult := tree.comparator(value, currentNode.value)
	if comparatorResult < 0 {
		return tree.search(value, currentNode.left)
	} else if comparatorResult > 0 {
		return tree.search(value, currentNode.right)
	} else {
		return currentNode
	}
}

func (tree *TreeSet) minimum(currentNode *node) *node {
	if currentNode.left == nil {
		return currentNode
	}
	return tree.minimum(currentNode.left)
}

func (tree *TreeSet) maximum(currentNode *node) *node {
	if currentNode.right == nil {
		return currentNode
	}
	return tree.maximum(currentNode.right)
}

func (tree *TreeSet) remove(value interface{}) bool {
	nodeToRemove := tree.search(value, tree.root)
	if nodeToRemove == nil {
		return false
	}

	if nodeToRemove.left == nil && nodeToRemove.right == nil {
		tree.size--
		if nodeToRemove.parent == nil {
			tree.root = nil
		} else {
			if nodeToRemove == nodeToRemove.parent.left {
				nodeToRemove.parent.left = nil
			} else {
				nodeToRemove.parent.right = nil
			}
		}
	} else if nodeToRemove.left == nil || nodeToRemove.right == nil {
		tree.size--
		if nodeToRemove.left != nil {
			if nodeToRemove.parent == nil {
				nodeToRemove.left.parent = nil
				tree.root = nodeToRemove.left
			} else {
				if nodeToRemove == nodeToRemove.parent.left {
					nodeToRemove.parent.left = nodeToRemove.left
				} else {
					nodeToRemove.parent.right = nodeToRemove.left
				}
			}
		} else {
			if nodeToRemove.parent == nil {
				nodeToRemove.right.parent = nil
				tree.root = nodeToRemove.right
			} else {
				if nodeToRemove == nodeToRemove.parent.left {
					nodeToRemove.parent.left = nodeToRemove.right
				} else {
					nodeToRemove.parent.right = nodeToRemove.right
				}
			}
		}
	} else {
		nextNode := tree.minimum(nodeToRemove.right)
		changedValue := nextNode.value
		tree.remove(nextNode.value)
		nodeToRemove.value = changedValue
	}
	return true
}
