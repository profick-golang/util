package treeset

type node struct {
	parent *node
	left   *node
	right  *node
	value  interface{}
}
